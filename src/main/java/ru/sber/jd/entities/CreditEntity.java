package ru.sber.jd.entities;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Data
@Entity(name = "credit")
public class CreditEntity {

    @Id
    @Column
    @GenericGenerator(name="generator", strategy="increment")
    @GeneratedValue(generator="generator")
    private Integer id;
    @Column(name="gosb_id")
    private Integer gosbId;
    @Column(name="gosb_name")
    private String gosbName;
    @Column(name="tb_name")
    private String tbName;
    @Column(name="credit")
    private Double creditValue;


}
