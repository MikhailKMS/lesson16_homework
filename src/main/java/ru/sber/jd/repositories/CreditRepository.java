package ru.sber.jd.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.sber.jd.entities.CreditEntity;

@Repository
public interface CreditRepository extends JpaRepository<CreditEntity, Integer> {

}
