package ru.sber.jd.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.sber.jd.entities.FotEntity;

@Repository
public interface FotRepository extends JpaRepository<FotEntity, Integer> {


}
