package ru.sber.jd.services;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;
import ru.sber.jd.entities.CreditEntity;
import ru.sber.jd.entities.FotEntity;
import ru.sber.jd.repositories.CreditRepository;
import ru.sber.jd.repositories.FotRepository;

@Service
@RequiredArgsConstructor
public class MainService implements CommandLineRunner {

    private final CreditRepository creditRepository;
    private final FotRepository fotRepository;


    @Override
    public void run(String... args) throws Exception {

        CreditEntity creditEntity = new CreditEntity();

        creditEntity.setGosbId(8610);
        creditEntity.setGosbName("Банк Татарстан");
        creditEntity.setTbName("ВВБ");
        creditEntity.setCreditValue(1000000D);

        CreditEntity save = creditRepository.save(creditEntity);

        FotEntity fotEntity = new FotEntity();

        fotEntity.setGosbId(8610);
        fotEntity.setGosbName("Банк Татарстан");
        fotEntity.setTbName("ВВБ");
        fotEntity.setFotValue(200000D);

        CreditEntity cr = new CreditEntity();
        cr.setId(save.getId());
        fotEntity.setCreditId(cr);

        fotRepository.save(fotEntity);

        //Programm result
        System.out.println(creditRepository.findAll());
        System.out.println(fotRepository.findAll());

    }



}
